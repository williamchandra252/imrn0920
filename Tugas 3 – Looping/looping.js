//SOAL 1 LOOPING WHILE
console.log("SOAL 1 LOOPING WHILE\n");

console.log("LOOPING PERTAMA");
a = 2;
while(a < 21){
    console.log(a + " - I love coding");
    a += 2;
}
a = 20;
console.log("LOOPING KEDUA");
while(a > 0){
    console.log(a + " - I will become a mobile developer");
    a -= 2;
}

console.log("\n-----------------------------------------\n");

//SOAL 2
console.log("SOAL 2 LOOPING FOR\n");

for (var i = 1; i < 21; i++) {
    if ( i % 2 == 0) {
        console.log(i + " - Berkualitas");
    }else{
        if( i % 3 == 0){
            console.log(i + " - I Love Coding ");
        }else{
            console.log(i + " - Santai");
        }
    }
}

console.log("\n-----------------------------------------\n");

//SOAL 3
console.log("SOAL 3 Membuat Persegi Panjang\n");

for (var i = 0; i < 4; i++) {
    var pesan = "";
    for (var j = 0; j < 8; j++) {
        pesan += "#";
    }
    console.log(pesan);
}

console.log("\n-----------------------------------------\n");

//SOAL 4
console.log("SOAL 4 Membuat Tangga\n");

for (var i = 1 ; i <= 7 ; i++){
    var pesan = "";
    for (var j = 1; j <= i; j++) {
        pesan += "#";
    }
    console.log(pesan);
}

console.log("\n-----------------------------------------\n");

//SOAL 5
console.log("SOAL 5 Membuat Papan Catur\n");

for (var i = 1 ; i <= 8 ; i++){
    if ( i % 2 == 0) {
        console.log("# # # #");
    }else{
        console.log(" # # # #");
    }
}

console.log("\n-----------------------------------------\n");