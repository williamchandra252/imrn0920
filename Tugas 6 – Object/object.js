// SOAL 1 (Array to Object)
console.log("SOAL 1 (Array to Object)\n");

function arrayToObject(arr) {
    var now = new Date()

    for (var i = 0; i < arr.length; i++) {
        var obj = {}
        obj.firstName = arr[i][0]
        obj.lastName = arr[i][1]
        obj.gender = arr[i][2]

        if( now.getFullYear() > arr[i][3] ){
            obj.age = now.getFullYear() - arr[i][3]
        }
        else{
            obj.age = 'Invalid birth year'
        }
        
        console.log(i+1 + ". " + obj.firstName + " " + obj.lastName + ": ", obj)
    }
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

arrayToObject([]) // ""

console.log("\n-----------------------------------------\n");

// SOAL 2 (Shopping Time)
console.log("SOAL 2 (Shopping Time)\n");

function shoppingTime(memberId = "null", money) {
    if(memberId == '' || memberId == "null"){
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    }
    else if(money < 50000){
        return 'Mohon maaf, uang tidak cukup'
    }
    else{
        var leftovermoney = money
        var arrBelanja = []

        if(leftovermoney >= 1500000){
            arrBelanja.push('Sepatu Stacattu')
            leftovermoney -= 1500000
        } 
        if(leftovermoney >= 500000){
            arrBelanja.push('Baju Zoro')
            leftovermoney -= 500000
        }
        if(leftovermoney >= 250000){
            arrBelanja.push('Baju H&N')
            leftovermoney -= 250000
        }
        if(leftovermoney >= 175000){
            arrBelanja.push('Sweater Uniklooh')
            leftovermoney -= 175000
        }
        if(leftovermoney >= 50000){
            arrBelanja.push('Casing Handphone')
            leftovermoney -= 50000
        }
    }

    var obj = {
        memberId: memberId,
        money: money,
        listPurchased: arrBelanja,
        changeMoney: leftovermoney
    }

    return obj
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime()); 

console.log("\n-----------------------------------------\n");

// SOAL 3 (Naik Angkot)
console.log("SOAL 3 (Naik Angkot)\n");

function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var listPenumpang = []
    
    for (var i = 0; i < arrPenumpang.length; i++) {
        var nama = arrPenumpang[i][0]
        var ruteAwal = arrPenumpang[i][1]
        var ruteAkhir = arrPenumpang[i][2]
        
        var count = rute.slice(rute.indexOf(ruteAwal), rute.indexOf(ruteAkhir)).length
        var bayar = count * 2000

        obj = {
            penumpang: nama,
            naikDari: ruteAwal,
            tujuan: ruteAkhir,
            bayar: bayar
        }        
        listPenumpang.push(obj)
    }
    return listPenumpang
}

  //TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]

console.log("\n-----------------------------------------\n");