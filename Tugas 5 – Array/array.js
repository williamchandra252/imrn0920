// SOAL 1 (Range)
console.log("SOAL 1 (Range)\n");

function range(startNum, finishNum) {
    var arr = Array();

    if(startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i++) {
            arr.push(i);
        }
        return arr
    }
    else if(startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i--) {
            arr.push(i);
        }
        return arr
    }
    else { return -1 }
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54, 50));
console.log(range());

console.log("\n-----------------------------------------\n");

// SOAL 2 (Range with Step)
console.log("SOAL 2 (Range with Step)\n");

function rangeWithStep(startNum, finishNum, step) {
    var arr = Array();

    if(startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i += step) {
            arr.push(i);
        }
        return arr;
    }
    else if(startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i -= step) {
            arr.push(i);
        }
        return arr;
    }
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

console.log("\n-----------------------------------------\n");

// SOAL 3 (Sum of Range)
console.log("SOAL 3 (Sum of Range)\n");

function sum(startNum="null", finishNum="null", step = 1){
    var jumlah = 0;

    if(startNum == "null" && finishNum == "null"){
        jumlah = 0
    }
    else if(startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i += step) {
            jumlah += i;
        }
    }
    else if(startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i -= step) {
            jumlah += i;
        }
    }
    else if (finishNum == "null"){
        jumlah = startNum
    }


    return jumlah;
}

console.log(sum(1,10))
console.log(sum(5, 50, 2))
console.log(sum(15,10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())

console.log("\n-----------------------------------------\n");

// SOAL 4 (Array Multidimensi)
console.log("SOAL 4 (Array Multidimensi)\n");

function datahandling(array) {
    for (var i = 0; i < array.length; i++) {
        console.log("Nomor ID:  " + array[i][0])
        console.log("Nama Lengkap:  " + array[i][1])
        console.log("TTL:  " + array[i][2] + " " + array[i][3])
        console.log("Hobi:  " + array[i][4])
        console.log("\n")
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

datahandling(input)

console.log("\n-----------------------------------------\n");

// SOAL 5 (Balik Kata)
console.log("SOAL 5 (Balik Kata)\n");

function balikKata(kata){
    var kataTerbalik = ''
    for (var i = kata.length -1 ; i >= 0 ; i--){
        kataTerbalik += kata[i]
    }

    return kataTerbalik
}

console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah")) 
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

console.log("\n-----------------------------------------\n");

// SOAL 6 (Metode Array)
console.log("SOAL 6 (Metode Array)\n");

function dataHandling2(array){
    newarr = array

    newarr.splice(1, 1, newarr[1] + "Elsharawy")
    newarr.splice(2, 1, "Provinsi " + newarr[2])
    newarr.splice(4, 1, "Pria")
    newarr.splice(5, 0, "SMA Internasional Metro")

    console.log(newarr)

    var split = newarr[3].split("/")
    var joined = split.join("-")
    var bulan = split[1]

    switch(bulan) {
        case '01':   { console.log(" Januari"); break; }
        case '02':   { console.log(" Februari "); break; }
        case '03':   { console.log(" Maret "); break; }
        case '04':   { console.log(" April "); break; }
        case '05':   { console.log(" Mei "); break; }
        case '06':   { console.log(" Juni "); break; }
        case '07':   { console.log(" Juli "); break; }
        case '08':   { console.log(" Agustus "); break; }
        case '09':   { console.log(" September "); break; }
        case '10':   { console.log(" Oktober "); break; }
        case '11':   { console.log(" November "); break; }
        case '12':   { console.log(" Desember "); break; }
        default  :   { console.log('Bulan tidak ditemukan!'); }
    }
    
    var sorted = split.sort(function (value1, value2) { return value2 - value1 })
    console.log(sorted)

    console.log(joined)

    var sliced = newarr[1].slice(0, 15)
    console.log(sliced)
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

console.log("\n-----------------------------------------\n");