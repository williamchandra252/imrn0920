// IF - ELSE
console.log("IF-ELSE\n");

var nama = "Albet";
var peran = "Werewolf";

if(nama == ""){
    console.log("Nama harus diisi!");
}
else if(peran == ""){
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
}
else{
    var pesan = "";
    if(peran == "Penyihir"){
        pesan = "Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!";
    } else if(peran == "Guard"){
        pesan = "Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.";
    } else{
        pesan = "Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!";
    }
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log(pesan);
}

console.log("\n-----------------------------------------\n");

// SWITCH CASE
console.log("SWITCH CASE\n");

var tanggal = 21; 
var bulan = 11; 
var tahun = 1945;

switch(bulan) {
    case 1:   { console.log(tanggal + " Januari " + tahun); break; }
    case 2:   { console.log(tanggal + " Februari " + tahun); break; }
    case 3:   { console.log(tanggal + " Maret " + tahun); break; }
    case 4:   { console.log(tanggal + " April " + tahun); break; }
    case 5:   { console.log(tanggal + " Mei " + tahun); break; }
    case 6:   { console.log(tanggal + " Juni " + tahun); break; }
    case 7:   { console.log(tanggal + " Juli " + tahun); break; }
    case 8:   { console.log(tanggal + " Agustus " + tahun); break; }
    case 9:   { console.log(tanggal + " September " + tahun); break; }
    case 10:  { console.log(tanggal + " Oktober " + tahun); break; }
    case 11:  { console.log(tanggal + " November " + tahun); break; }
    case 12:  { console.log(tanggal + " Desember " + tahun); break; }
    default:  { console.log('Bulan tidak ditemukan!'); }
}
