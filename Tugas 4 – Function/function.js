//SOAL 1 
console.log("SOAL 1\n");

function teriak() {
    return "Halo Sanbers!";
}

console.log(teriak());

console.log("\n-----------------------------------------\n");

//SOAL 2
console.log("SOAL 2\n");

function kalikan(angka1, angka2) {
    return angka1 * angka2;
}
    var num1 = 12;
    var num2 = 4;
 
    var hasilKali = kalikan(num1, num2);
    console.log(hasilKali);

console.log("\n-----------------------------------------\n");

//SOAL 3
console.log("SOAL 3\n");

function introduce(name, age, address, hobby) {
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!"
}

var name = "Albet"
var age = 21
var address = "Jln. Delima 2"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan);

console.log("\n-----------------------------------------\n");